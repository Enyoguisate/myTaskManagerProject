﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace myProjectApp
{
    public class Comments
    {
        public int Task_Id { get; set; }
        public int Comment_Id { get; set; }
        public string Comment_Text { get; set; }

    }
}