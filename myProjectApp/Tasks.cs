﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace myProjectApp
{
    public class Tasks
    {
        public int Projec_Id { get; set; }
        public int Task_Id { get; set; }
        public string Task_Name { get; set; }
        public DateTime Task_Start_Date { get; set; }
        public DateTime Task_End_Date { get; set; }
        public int User_Id { get; set; }
        public int Task_Creator_User_Id { get; set; }
        public int Task_Status_Id { get; set; }
        public List<Comments> Comments { get; set; }
    }
}