﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace myProjectApp
{
    public class Users
    {
        public int User_Id { get; set; }
        public string User_Name { get; set; }
        public string User_First_Name { get; set; }
        public string User_Last_Name { get; set; }
        public string User_Password { get; set; }
        public string User_Email { get; set; }
    }
}