﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using myProjectApp.DAL;
using myProjectApp;

namespace myProjectApp.BLL
{
    public class ProjectBLL
    {
        private ProjectDAL projectDAL;

        public ProjectBLL()
        {
            projectDAL = new ProjectDAL();
        }

        
        public List<Project> GetAll()
        {
            return projectDAL.GetAllProjects();
        }

        public List<Project>GetById(int id)
        {
            return projectDAL.GetProjectById(id);
        }
        public bool CreateProject(Project aProject)
        {
            return projectDAL.InsertProject(aProject);
        }

        public bool UpdateProject(Project aProject)
        {
            return projectDAL.UpdProject(aProject);
        }
        public bool ValExist(Project aProject)
        {
            return projectDAL.ValidateProjectExistence(aProject);
        }
        public int GetLastId()
        {
            return projectDAL.GetIdToIns();
        }

    }
}