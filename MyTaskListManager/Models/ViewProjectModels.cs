﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyTaskListManager.Models
{
    public class ViewProjectModels
    {
        public ViewProjectModels()
        {
            ProjectModelList = new List<ProjectModels>();
        }
            
       public List<ProjectModels> ProjectModelList { get; set; }
    }
}