﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyTaskListManager.Models
{
    public class ProjectModels
    {
        public int Project_Id { get; set; }

        [DisplayName("Name")]
        public string Project_Name { get; set; }

        [DisplayName("Description")]
        public string Project_Description { get; set; }

        [DisplayName("Creation Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Project_Created_Time { get; set; }

        [DisplayName("Start Date")]
        public DateTime Project_Start_Date { get; set; }

        [DisplayName("End Date")]
        public DateTime Project_End_Date { get; set; }

        [DisplayName("User Count")]
        public int User_Count { get; set; }

    }
}