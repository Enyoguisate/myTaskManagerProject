﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using myProjectApp;
using myProjectApp.BLL;


namespace MyTaskListManager.Controllers
{
    public class ProjectController : Controller
    {

        [HttpGet]
        public ActionResult AddProject()
        {
            ViewBag.Title = "AddProject";
            //var model = new List<Project>();
            //ProjectBLL objMock = new ProjectBLL();
            //model = objMock.GetAll();
            return View("AddProject");
        }
        [HttpGet]
        public ActionResult LoadProjectsDataTable()
        {
            var model = new List<Project>();
            ProjectBLL objMock = new ProjectBLL();
            model = objMock.GetAll();
            return View(model);
        }

        [HttpPost]
        public ActionResult AddProject(Project model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    myProjectApp.Project objMock = new myProjectApp.Project();
                    objMock.Project_Name = model.Project_Name;
                    objMock.Project_Description = model.Project_Description;
                    objMock.Project_Created_Time = model.Project_Created_Time;
                    objMock.Project_Start_Date = model.Project_Start_Date;
                    objMock.Project_End_Date = model.Project_End_Date;
                    objMock.User_Count = model.User_Count;

                    ProjectBLL objBLLMock = new ProjectBLL();

                    try
                    {
                        var ret = objBLLMock.CreateProject(objMock);
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }                
            }
            catch (Exception)
            {
                return View(model);                
            }
            ModelState.Clear();
            //return View("LoadProjectsDataTable");
            return RedirectToAction("LoadProjectsDataTable");
        }

        // GET: Project/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Project/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Project/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Project/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Project/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Project/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Project/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
