﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;
using System.Text;

namespace myProjectApp.DAL
{
	public class myProjectApp_DataBase
	{
        public const string CONNECTION_NAME = "myDbConection";
        public static string _connectionString = String.Empty;
		protected SqlConnection Connection { get; private set; }
		public static string ConnectionString
        {
            get
            {
				if(_connectionString == String.Empty)
                {
                    _connectionString = WebConfigurationManager.ConnectionStrings[CONNECTION_NAME].ConnectionString;
                }
                return _connectionString;
            }
        }
		public myProjectApp_DataBase()
        {
			if(this.Connection == null)
            {
                this.Connection = new SqlConnection(ConnectionString);
            }
        }
		public SqlCommand GetCommand(string commandText, CommandType commandType)
        {
            OpenConnection();
            return new SqlCommand(commandText, this.Connection)
            {
				CommandType = commandType
            };
        }
		public void CloseConnection()
        {
            if (this.Connection.State != ConnectionState.Closed)
                this.Connection.Close();
        }
		private void OpenConnection()
        {
            if (this.Connection.State != ConnectionState.Open)
                this.Connection.Open();
        }
		public int GetInsertId(string pk, string table)
        {
            int id = 0;
            try
            {
                StringBuilder query = new StringBuilder();
                query.AppendFormat("select max("+pk+") as MAXID from "+table, pk, table);
				using (var cmd = GetCommand(query.ToString(), CommandType.Text))
                {
                    var reader = cmd.ExecuteReader();
                    if (!reader.HasRows)
                        return id;
					int maxID=reader.GetOrdinal("MAXID");
					while (reader.Read())
                    {
                        return reader.GetInt32(maxID) + 1;
                    }

                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                CloseConnection();
            }
            return id;
        }
	}
}