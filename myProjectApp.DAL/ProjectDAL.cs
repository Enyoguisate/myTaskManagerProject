﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using myProjectApp;


namespace myProjectApp.DAL
{
    public class ProjectDAL : myProjectApp_DataBase
    {
        public ProjectDAL() : base()
        {

        }

        public List<Project> GetAllProjects()
        {
            try
            {
                using(var cmd = GetCommand("[SP_GET_PROJECTBYID]", CommandType.StoredProcedure))
                {
                    using(var reader = cmd.ExecuteReader())
                    {
                        if (!reader.HasRows)
                            return null;
                        int idxID = reader.GetOrdinal("Project_Id");
                        int namexName = reader.GetOrdinal("Project_Name");
                        int descriptionxDescription = reader.GetOrdinal("Project_Description");
                        int createddatexCreatedDate = reader.GetOrdinal("Project_Created_Date");
                        int startdatexStartDate = reader.GetOrdinal("Project_Start_Date");
                        int enddatexEndDate = reader.GetOrdinal("Project_End_Date");

                        List<Project> projectList = new List<Project>();
                        while (reader.Read())
                        {
                            Project project = new Project();
                            project.Project_Id = reader.GetInt32(idxID);
                            project.Project_Name = reader.GetString(namexName);
                            project.Project_Description = reader.GetString(descriptionxDescription);
                            project.Project_Created_Time = reader.GetDateTime(createddatexCreatedDate);
                            project.Project_Start_Date = reader.GetDateTime(startdatexStartDate);
                            project.Project_End_Date = reader.GetDateTime(enddatexEndDate);

                            projectList.Add(project);
                        }
                        return projectList;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        

        public bool InsertProject(Project aProject)
        {
            bool aState = false;
            var project_id = GetInsertId("Project_Id", "Projects");
            try
            {
                using(var cmd = GetCommand("[SP_INS_PROJECT]", CommandType.StoredProcedure))
                {
                    cmd.Parameters.AddWithValue("@Project_Id", project_id);
                    cmd.Parameters.AddWithValue("@Project_Name", aProject.Project_Name);
                    cmd.Parameters.AddWithValue("@Project_Description", aProject.Project_Description);
                    cmd.Parameters.AddWithValue("@Project_Created_Date", aProject.Project_Created_Time);
                    cmd.Parameters.AddWithValue("@Project_Start_Date", aProject.Project_Start_Date);
                    cmd.Parameters.AddWithValue("@Project_End_Date", aProject.Project_End_Date);
                    cmd.ExecuteNonQuery();
                    aState = true;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                CloseConnection();
            }
            return aState;
        }

        public int GetIdToIns()
        {
            return GetInsertId("Project_Id", "Project");
        }

        public List<Project> GetProjectById(int id)
        {
            List<Project> projectList = new List<Project>();
            try
            {
                using (var cmd = GetCommand("[SP_GET_PROJECTBYID] @Project_Id={0}", CommandType.StoredProcedure))
                {

                    cmd.Parameters.AddWithValue("@Project_Id", SqlDbType.Int);

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (!reader.HasRows) return null;
                        int idxID = reader.GetOrdinal("Project_Id");
                        int namexName = reader.GetOrdinal("Project_Name");
                        int descriptionxDescription = reader.GetOrdinal("Project_Description");
                        int createddatexCreatedDate = reader.GetOrdinal("Project_Created_Date");
                        int startdatexStartDate = reader.GetOrdinal("Project_Start_Date");
                        int enddatexEndDate = reader.GetOrdinal("Project_End_Date");

                        while (reader.Read())
                        {
                            Project project = new Project();
                            project.Project_Id = reader.GetInt32(idxID);
                            project.Project_Name = reader.GetString(namexName);
                            project.Project_Description = reader.GetString(descriptionxDescription);
                            project.Project_Created_Time = reader.GetDateTime(createddatexCreatedDate);
                            project.Project_Start_Date = reader.GetDateTime(startdatexStartDate);
                            project.Project_End_Date = reader.GetDateTime(enddatexEndDate);

                            projectList.Add(project);
                        }
                        return projectList;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public bool UpdProject(Project aProject)
        {
            var aState = false;
            try
            {
                using (var cmd = GetCommand("[SP_UPD_PROJECT] @Project_Id=" + aProject.Project_Id, CommandType.StoredProcedure))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (!reader.HasRows)
                            return aState;
                        cmd.Parameters.AddWithValue("@Project_Id", aProject.Project_Id);
                        cmd.Parameters.AddWithValue("@Project_Name", aProject.Project_Name);
                        cmd.Parameters.AddWithValue("@Project_Description", aProject.Project_Description);
                        cmd.Parameters.AddWithValue("@Project_Created_Date", aProject.Project_Created_Time);
                        cmd.Parameters.AddWithValue("@Project_Start_Date", aProject.Project_Start_Date);
                        cmd.Parameters.AddWithValue("@Project_End_Date", aProject.Project_End_Date);
                        if (cmd.ExecuteNonQuery() > 0)
                        {
                            aState = true;
                        }
                    }
                }
                return aState;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        public bool ValidateProjectExistence(Project aProject)
        {
            bool aState = false;
            List<Project> projectList = GetProjectById(aProject.Project_Id);
            if (projectList.Count == 0)
            {
                return aState;
            }
            else
            {
                aState = true;
            }
            return aState;
        }
    }
}